<?php
namespace a161\Aimage;

use Exception;
use App\Services\Hp;

class Aimage
{
    protected $arFile;
    protected $srcImage;
    protected $dstImage;
    protected $srcWidth;
    protected $srcHeight;
    protected $_srcWidth;
    protected $_srcHeight;
    protected $dstWidth;
    protected $dstHeight;
    protected $srcType;
    protected $fileExtension;
    protected $srcX = 0;
    protected $srcY = 0;
    protected $dstX = 0;
    protected $dstY = 0;
    protected $wmFile;
    protected $wmImage;
    protected $wmWidth;
    protected $wmHeight;
    protected $wmImageType;
    protected $resizeWidth;
    protected $resizeHeight;

    const EXTENSIONS_ACCEPTED = ['jpg', 'jpeg', 'png', 'gif'];
    const RESIZE_MODES = ['auto', 'fix', 'crop'];

    public function __construct($file)
    {
        $arFile = $this->setFile($file);

        $this->loadImage(
            $this->srcWidth,
            $this->srcHeight,
            $this->srcType,
            $this->srcImage,
            $arFile['path']
        );
       
    }

    /**
     * @throws Exception
     */
    protected function setFile($file, $checkIfExist = true) : array
    {
        $tmp = explode('.', $file);
        $arFile['extension'] = array_pop($tmp);

        if (!in_array($arFile['extension'], self::EXTENSIONS_ACCEPTED)) {
            throw new Exception('Wrong file extension');
        }

        $file = str_replace('\\', '/', $file);
        $file = "/" . ltrim($file, "/");

        if (!file_exists($file) && isset($_SERVER['DOCUMENT_ROOT']) && $_SERVER['DOCUMENT_ROOT']) {
            $file = str_replace($_SERVER['DOCUMENT_ROOT'], "", $file);
            $file = $_SERVER['DOCUMENT_ROOT'] . $file;
        }

        if ($checkIfExist && !file_exists($file)) {
            throw new Exception('File ' . $file . ' does not exist');
        }

        $tmp = explode('/', $file);
        $arFile['name'] = array_pop($tmp);
        $arFile['src'] = str_replace($_SERVER['DOCUMENT_ROOT'], "", $file);
        $arFile['path'] = $file;

        return $arFile;
    }

    protected  function setSize(&$dstWidth, &$dstHeight, $mode)
    {
        $mode = in_array($mode, self::RESIZE_MODES) ? $mode : self::RESIZE_MODES[0];

        $imageRatio = $this->srcWidth / $this->srcHeight;
        $dstRatio = $dstWidth / $dstHeight;
        $this->_srcWidth = $this->srcWidth;
        $this->_srcHeight = $this->srcHeight;

        switch ($mode) {

            case 'auto':

                if ($imageRatio >= $dstRatio) {
                    $this->dstWidth = $dstWidth;
                    $dstHeight = $this->dstHeight = $this->dstWidth / $imageRatio;
                } else {
                    $this->dstHeight = $dstHeight;
                    $dstWidth = $this->dstWidth = $this->dstHeight * $imageRatio;
                }

                break;

            case 'fix':
                
                if ($imageRatio >= $dstRatio) {
                    $this->dstX = 0;
                    $this->dstHeight = $dstWidth / $imageRatio;
                    $this->dstY = abs($dstHeight - $this->dstHeight) / 2;
                    $this->dstWidth = $dstWidth;
                } else {
                    $this->dstY = 0;
                    $this->dstWidth = $dstHeight * $imageRatio;
                    $this->dstX = abs($dstWidth - $this->dstWidth) / 2;
                    $this->dstHeight = $dstHeight;
                }
                
                break;

            case 'crop':

                $this->dstHeight = $dstHeight;
                $this->dstWidth = $dstWidth;

                if ($imageRatio >= $dstRatio) {
                    $this->_srcHeight = $this->srcHeight;
                    $this->_srcWidth = $this->srcHeight * $dstRatio;
                    $this->srcX = abs($this->srcWidth - $this->dstWidth) / 2;
                    $this->srcY = 0;
                } else {
                    $this->_srcWidth = $this->srcWidth;
                    $this->_srcHeight = $this->srcWidth / $dstRatio;
                    $this->srcX = 0;
                    $this->srcY = abs($this->srcHeight - $this->dstHeight) / 2;
                }
                
                break;
        }
    }


    public function resize($dstWidth, $dstHeight, $mode = 'auto', $transp = false)
    {
        $this->setSize($dstWidth, $dstHeight, $mode);

        $this->dstImage = imagecreatetruecolor($dstWidth, $dstHeight);

        $white = imagecolorallocate($this->dstImage, 255, 255, 255);

        imagefill($this->dstImage, 0, 0, $white);

        imagecopyresampled(
            $this->dstImage,
            $this->srcImage,
            $this->dstX,
            $this->dstY,
            $this->srcX,
            $this->srcY,
            $this->dstWidth,
            $this->dstHeight,
            $this->_srcWidth,
            $this->_srcHeight
        );

        imagedestroy ($this->srcImage);

        return $this;
    }

    public function waterMark($file, $position = 22, $offset = 0)
    {
        $position = (string)$position;

        $arFile = $this->setFile($file, false);

        $this->loadImage(
            $this->wmWidth,
            $this->wmHeight,
            $this->wmImageType,
            $this->wmImage,
            $arFile['path']
        );

        switch($position)
        {
            case '00':
                $dx = $offset;
                $dy = $offset;
                break;
            case '01':
                $dx = $offset;
                $dy = ($this->dstHeight - $this->wmHeight) / 2 ;
                break;
            case '02':
                $dx = $offset;
                $dy = $this->dstHeight - $this->wmHeight - $offset;
                break;
            case '11':
                $dx = ($this->dstWidth - $this->wmWidth) / 2;
                $dy = ($this->dstHeight - $this->wmHeight) / 2;
                break;
            case '12':
                $dx = ($this->dstWidth - $this->wmWidth) / 2;
                $dy = $this->dstHeight - $this->wmHeight - $offset;
                break;
            case '20':
                $dx = $this->dstWidth - $this->wmWidth - $offset;
                $dy = self::WM_OFFSET;
                break;
            case '10':
                $dx = ($this->dstWidth - $this->wmWidth) / 2;
                $dy = self::WM_OFFSET;
                break;
            case '22':
            default:
                $dx = $this->dstWidth - $this->wmWidth - $offset;
                $dy = $this->dstHeight - $this->wmHeight - $offset;
                break;
        }

        imagecopy($this->dstImage, $this->wmImage, $dx, $dy, 0, 0, $this->wmWidth, $this->wmHeight);

        imagedestroy ($this->wmImage);

        return $this;
    }

    public function save($file = false, $quality = 0)
    {
        $this->arFile = $file ? $this->setFile($file, false) : $this->arFile;

        if ($this->dstImage) {
            switch($this->fileExtension)
            {
                case 'jpg':
                    $quality = ($quality <= 1 || $quality >= 100)? 90 : $quality;
                    ImageJpeg($this->dstImage, $this->arFile['path'], $quality);
                    break;

                case 'png':
                    $quality = ($quality < 1 || $quality > 9)? 9 : $quality;
                    ImagePng($this->dstImage, $this->arFile['path'], $quality);
                    break;

                case 'gif':
                    ImageGif($this->dstImage, $this->arFile['path']);
                    break;
            }
        } else {
            throw new \Exception('Create image error');
        }

        imagedestroy ($this->dstImage);

        return $this;
    }
    
    private function loadImage(&$srcWidth, &$srcHeight, &$srcType, &$srcImage, $file)
    {
        list($srcWidth, $srcHeight, $srcType) = getimagesize($file);
        
        switch ($srcType)
        {
            case 1:
                $srcImage = imagecreatefromgif($file);
                $this->fileExtension = 'gif';
                break;
                
            case 2:
                $srcImage = imagecreatefromjpeg($file);
                $this->fileExtension = 'jpg';
                break;
                
            case 3:
                $srcImage = imagecreatefrompng($file);
                $this->fileExtension = 'png';
                break;
                
            default:
                throw new Exception('Unsupported image type');
        }
        if (!$srcImage) {
            throw new Exception('Cannot load the file ' . $file);
        }
    }
    
    public function getImage()
    {
        return $this->arFile;
    }
}